//Author sabarinath.g : g.sabarinath91@gmail.com
package main

import (
        "bufio"
        "fmt"
        "log"
        "os"
        "strings"
)

//function to filter duplicate timestamp
func duptimestamp(list []string) []string {
        duptimestamp_hash := make(map[string]int)
        for _, item := range list {
                _, exist := duptimestamp_hash[item]
                if exist {
                        duptimestamp_hash[item] += 1
                } else {
                        duptimestamp_hash[item] = 1
                }
        }
        var filterdup_timestamp []string
        for key, value := range duptimestamp_hash {
                if value > 1 {
                        filterdup_timestamp = append(filterdup_timestamp, key)
                }
        }
        return filterdup_timestamp
}

// function to check file descriptor error
func check(e error) {
        if e != nil {
                panic(e)
        }
}

// funtion to write slice string to a file
func WrtToFile(content []string) {
        outfile, err := os.Create("c:\\Users\\Sabarinath.G\\Desktop\\apache_out.txt")
        check(err)
        defer outfile.Close()
        w := bufio.NewWriter(outfile)
        contentStr := strings.Join(content, "  ")
        fmt.Printf("Writing duplicate timestamp result to a file: %s ", outfile.Name())

        w.WriteString(contentStr)

}

func main() {
        scanner := bufio.NewScanner(os.Stdin)
        fmt.Println("Enter the log filename with location(ex:/home/access_log):")
        scanner.Scan()
        filename := scanner.Text()
        fmt.Printf("filename : %s \n", filename)
        logfile, err := os.Open(filename)
        if err != nil {
                log.Fatal(err)
                fmt.Println(err)
        }
        defer logfile.Close()

        var logdata []string
        logscanner := bufio.NewScanner(logfile)
        //scanner.Split(bufio.ScanLines)
        for logscanner.Scan() {
                logdata = append(logdata, logscanner.Text())
        }

        var timestamp []string

        for _, line := range logdata {
                logparse := strings.Fields(line)
                timestamp = append(timestamp, strings.TrimLeft(logparse[3], "["))
        }
        result := duptimestamp(timestamp)
        fmt.Printf("Duplicate timestamp count:%d\n", len(result))
        WrtToFile(result)
}
