# README #

https://geekcontainer.wordpress.com/2018/05/09/code-to-parse-a-large-apache-http-access-log/

### What is this repository for? ###

* Quick summary
	Parse apache httpd access logs file and filter timestamp alone that is repeated or present multiple times.Save the timestamp result to a file.
* Version
    1.0
* g.sabarinath91@gmail.com

### How do I get set up? ###

* Summary of set up
       Clone the repo "git clone https://bitbucket.org/gsabarinath91/parse_apache_log_filter_duplicate_timestamp.git".
	   Repo has 4 files README,sample log file,python script,go code.
	   python and go code written to parse the log file and write result to a file
* How to run tests
       
	   python parse_duplicate_timestamp_in_log
	   
	   go run parse_duplicate_timestamp_in_log.go
   
   result would print like,


Enter the log filename with location(ex:/home/access_log):C:\\Users\\Sabarinath.G\\Desktop\\apache_access_log.txt
filename : C:\\Users\\Sabarinath.G\\Desktop\\apache_access_log.txt
Writing duplicate timestamp result to a file: c:\Users\Sabarinath.G\Desktop\apache_out.txt 
Duplicate timestamp count:7326


### Contribution guidelines ###
* utilize concurrency model from python and go to work with large file paring and producing result faster.

